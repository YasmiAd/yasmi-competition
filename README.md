# Competiton Project
Eerste examenopdracht Programmeren 4
1. Maak een workspace met de naam voornaam-competition
2. Maak een betekenisvolle README pagina
3. Maak het model
4. Maak de views en de css
5. Maak de controller, voorlopig met een switch statement

## Support & Documentation

1. Voor de __leerstof__ zie [Modern Ways Programmeren 4] (https://www.modernways.be/myap/it/school/lesson/programmeren4/Programmeren%204%20Orde%20in%20PHP.html)
2. Voor **Markdown** syntaxis zie [Markdown cheat sheet] (https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



# Stappen

1. composer.json
2. run composer dumpautoload
3. maak de model klassen voor Modernways\Competition
4. Maak de views voor Modernways\Competition
5. Maak de controller voor ModernWays\Competition
