<?php
namespace ModernWays\Competitie\Model;

class Team
{
    private $name;
    private $location;
    private $Points;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getPoints()
    {
        return $this->Points;
    }

    /**
     * @param mixed $Points
     */
    public function setPoints($Points)
    {
        $this->Points = $Points;
    }
}