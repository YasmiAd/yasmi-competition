<?php
namespace ModernWays\Competitie\Model;

class Player
{
    private $lastName;
    private $firstName;
    private $address;
    private $shirtnumber;

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getShirtnumber()
    {
        return $this->shirtnumber;
    }

    /**
     * @param mixed $shirtnumber
     */
    public function setShirtnumber($shirtnumber)
    {
        $this->shirtnumber = $shirtnumber;
    }

}