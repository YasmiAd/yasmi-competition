<?php
namespace ModernWays\Competition\Model;

class Liga
{
    private $name;
    private $year;
    private $isInPlanning;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year): void
    {
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getisInPlanning()
    {
        return $this->isInPlanning;
    }

    /**
     * @param mixed $isInPlanning
     */
    public function setIsInPlanning($isInPlanning): void
    {
        $this->isInPlanning = $isInPlanning;
    }


}